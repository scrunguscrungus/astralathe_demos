# Astralathe Demos

In this repository you can find a variety of demo mods that demonstrate numerous features and functions of [Astralathe](https://gitlab.com/scrunguscrungus/astralathe).
The primary intention of this is to provide a repository of examples that can help understand how to create Psychonauts mods.

In each mod's folder you can find a description detailing the name of the mod, what it demonstrates and what the mod does in-game.